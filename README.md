# MyBnB - 1.0

Hello,

Thank you for your interest into my works. You can find in this folder a project I did alone.
I also prefer to show one project build from A to Z with all the flows instead of many different small projects.

This project is part of my portfolio.

For a better experience, I recommend to open files on Desktop (especially the prototype). Design files open in browser (Figma), so you can access the files from any devices with a browser (Google Chrome is recommended).  I would recommend to open `Project Presentation.pdf` file first and then navigate through the files and designs.

Please download the content of the repository to consult files.

Have a good day

Lionel Thirion

## Links 


### Design File
https://www.figma.com/file/N8DMuLDTMVKwHuqnRRgIWA/MyBnb-1.0?node-id=22%3A429
### Prototype
https://www.figma.com/proto/N8DMuLDTMVKwHuqnRRgIWA/MyBnb-1.0?node-id=606%3A6500&viewport=266%2C472%2C0.09454470872879028&scaling=scale-down
